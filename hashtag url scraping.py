from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
import getpass
import time 
import csv
import re

username_ = input('username:')
password_ = getpass.getpass('password:')
keyword = input('hashtag(enter without #):')
keyword = '#'+keyword

driver = webdriver.Chrome("C:/Users/muhammad.rizqi/Downloads/chromedriver.exe")
driver.get("http://www.instagram.com")

username = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='username']")))
password = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name='password']")))

username.clear()
username.send_keys(username_)
password.clear()
password.send_keys(password_)

button = WebDriverWait(driver, 2).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "button[type='submit']"))).click()

not_now = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//button[contains(text(), "Not Now")]'))).click()
not_now2 = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, '//button[contains(text(), "Not Now")]'))).click()

searchbox = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.XPATH, "//input[@placeholder='Search']")))
searchbox.clear()

searchbox.send_keys(keyword)

time.sleep(5)
searchbox.send_keys(Keys.ENTER)
time.sleep(1)
searchbox.send_keys(Keys.ENTER)
time.sleep(5)

xpath = "//a[@role='link']"
element = [[], []]
for i in range(4):
    element[0] += driver.find_elements(By.XPATH, xpath)
    element[1] += driver.find_elements(By.CLASS_NAME, "_aagt")
    driver.execute_script("window.scrollTo(0, 10000);")
element[0] = element[0][:110]
element[1] = element[1][:110]

feature = [[], []]
for i in range(len(element[0])):
    feature[0].append(element[0][i].get_attribute('href'))
    feature[1].append(element[1][i].get_attribute('src'))

post = 'https://www.instagram.com/p/'
        
for i in feature[0]:
    if re.search(post, i):
        pass
    else:
        feature[0].remove(i)

feature[0] = feature[0][:100]
feature[1] = feature[1][:100]

for i in feature[0]:
    print(i)
    print('-----------')
